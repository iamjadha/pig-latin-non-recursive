public class MyClass {
    
    /**
 1. Move the first letter of each word to the end of the word
 and then add "ay"
 2. translate a phrase: Hello world -> ellohay orldway
*/

    public static void main(String args[]) {

        doPigLatin("Hello");
        doPigLatin("Hello World");
        doPigLatin("Hello World! Jasmine");


    }
    
    
    
    public static void doPigLatin(String data){
        String resultingString = "";
        
        String [] strings = data.split(" ");
        for(String s : strings){
            String str = s;
            resultingString = doPigLatinLogic(str);
        }
        
        System.out.print(resultingString);
    }
    
    private static String doPigLatinLogic(String str){
            StringBuffer resultingBuffer = new StringBuffer("");

        
            char[] arr = str.toCharArray();
            char[] result = new char[str.length()];
            int i = 0;
         
            while(i<arr.length-1){
                result[i] = arr[i+1];
                i++;
            }
            result[arr.length-1] = arr[0];
            resultingBuffer.append(" ").append(String.valueOf(result)).append("ay");
            return resultingBuffer.toString();
    }
}
